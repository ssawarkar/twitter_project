#!/bin/bash
#rm ~/.ssh/known_hosts
./Build_Head_Node.sh  > Head_Node.log 2>&1 &
sleep 3
./Build_Node1.sh  > Node1.log 2>&1 &
sleep 3
./Build_Node2.sh  > Node2.log 2>&1 &
sleep 3
./Build_Node3.sh  > Node3.log 2>&1 &
sleep 3
./Build_Timeline_Collector.sh  > Timeline.log 2>&1 &
sleep 3

clear
echo "Please Wait while the Nodes are being created on Nectar..."

Q1=$(nova list | grep ACTIVE | wc -l)
while [ $Q1 -lt 5 ]; do sleep 3; Q1=$(nova list | grep ACTIVE | wc -l); done

echo
sleep 150
echo "Please wait while the applications are being installed..."

Q2=$(ps -eaf | grep expect | grep -v grep| wc -l)
while [ $Q2 -gt 0 ]; do sleep 3; Q2=$(ps -eaf | grep expect | grep -v grep| wc -l); done


echo

echo "Please wait while the hostnames are being populated..."

IP_Node1=$(nova list | grep -i Node1 | awk '{print $12}' | awk -F"=" '{print $2}')
IP_Node2=$(nova list | grep -i Node2 | awk '{print $12}' | awk -F"=" '{print $2}')
IP_Node3=$(nova list | grep -i Node3 | awk '{print $12}' | awk -F"=" '{print $2}')
IP_HeadNode=$(nova list | grep HeadNode | awk '{print $12}' | awk -F"=" '{print $2}')
IP_Timelines=$(nova list | grep Timelines | awk '{print $12}' | awk -F"=" '{print $2}')

echo $IP_Node1 db1 > IPs
echo $IP_Node2 db2 >> IPs
echo $IP_Node3 db3 >> IPs
echo $IP_HeadNode headnode >> IPs
echo $IP_Timelines timelines >> IPs

scp -i /root/.ssh/key  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no IPs  ubuntu@$IP_Node1:~/  >> log 2>&1
scp -i /root/.ssh/key  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no IPs  ubuntu@$IP_Node1:~/ >> log 2>&1
scp -i /root/.ssh/key  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no IPs  ubuntu@$IP_Node1:~/ >> log 2>&1
scp -i /root/.ssh/key  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no IPs  ubuntu@$IP_HeadNode:~/ >> log 2>&1
scp -i /root/.ssh/key  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no IPs  ubuntu@$IP_Timelines:~/ >> log 2>&1

ssh -i /root/.ssh/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@$IP_Node1 "sudo cat IPs >> /etc/hosts" >> log 2>&1 &
ssh -i /root/.ssh/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@$IP_Node2 "sudo cat IPs >> /etc/hosts" >> log 2>&1 &
ssh -i /root/.ssh/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@$IP_Node3 "sudo cat IPs >> /etc/hosts" >> log 2>&1 &
ssh -i /root/.ssh/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@$IP_HeadNode "sudo cat IPs >> /etc/hosts" >> log 2>&1 &
ssh -i /root/.ssh/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@$IP_Timelines "sudo cat IPs >> /etc/hosts" >> log 2>&1 &

scp -i /root/.ssh/key  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no /root/.ssh/key  ubuntu@$IP_Node1:~/ >> log 2>&1 
scp -i /root/.ssh/key  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no /root/.ssh/key  ubuntu@$IP_Node1:~/ >> log 2>&1 
scp -i /root/.ssh/key  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no /root/.ssh/key  ubuntu@$IP_Node1:~/ >> log 2>&1
scp -i /root/.ssh/key  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no /root/.ssh/key  ubuntu@$IP_HeadNode:~/ >> log 2>&1
scp -i /root/.ssh/key  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no /root/.ssh/key  ubuntu@$IP_Timelines:~/ >> log 2>&1


echo

echo "Initializing the AWESOME.."
echo
#./Run_Scripts.sh
echo "All Systems UP, please use the IP Addresses below to access the platform, Please run Run_Scripts.sh to start collecting tweets.."
echo
nova list



