#!/bin/bash


IP_Node1=$(nova list | grep -i Node1 | awk '{print $12}' | awk -F"=" '{print $2}')
IP_Node2=$(nova list | grep -i Node2 | awk '{print $12}' | awk -F"=" '{print $2}')
IP_Node3=$(nova list | grep -i Node3 | awk '{print $12}' | awk -F"=" '{print $2}')
IP_HeadNode=$(nova list | grep -i HeadNode | awk '{print $12}' | awk -F"=" '{print $2}')
IP_Timelines=$(nova list | grep -i Timelines | awk '{print $12}' | awk -F"=" '{print $2}')



ssh -i /root/.ssh/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@$IP_Node1 "sudo /mnt/data/Dublin/Twitter_Engine.sh; sudo /mnt/data/Melbourne/Twitter_Engine.sh" &
ssh -i /root/.ssh/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@$IP_Node2 "sudo /mnt/data/Dublin/Twitter_Engine.sh; sudo /mnt/data/Melbourne/Twitter_Engine.sh" &
ssh -i /root/.ssh/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@$IP_Node3 "sudo /mnt/data/Dublin/Twitter_Engine.sh; sudo /mnt/data/Melbourne/Twitter_Engine.sh" &
ssh -i /root/.ssh/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@$IP_HeadNode "sudo /var/www/Graphs/Run_Graphs.sh" &
ssh -i /root/.ssh/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@$IP_Timelines "sudo /root/Dispatcher/Tweet_History/Dublin/Get_Tweet_History.sh ; sudo /root/Dispatcher/Tweet_History/Melbourne/Get_Tweet_History.sh" &
