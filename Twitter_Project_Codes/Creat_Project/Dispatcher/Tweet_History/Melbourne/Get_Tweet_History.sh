#!/bin/bash
while true; do
GET http://115.146.93.206:5984/twitterrawdossr3/_design/Tweet_Users/_view/all > Users_1
GET http://115.146.94.49:5984/twitterrawdossr2/_design/Tweet_Users/_view/all > Users_2
GET http://115.146.94.29:5984/twitterrawdossr1/_design/Tweet_Users/_view/all > Users_3
cat Users_1 Users_2 Users_3 > Users
cat Users  | awk -F"\"" '{print $8}' | awk '/./' | sort | uniq | grep -v null > All_Users_tmp

fgrep -vf Previous_Users All_Users_tmp > All_Users
mv All_Users_tmp Previous_Users



loading_DB=$(cat /mnt/data/loading)
while [ $loading_DB -eq 1 ]; do                         
sleep 3
loading_DB=$(cat /mnt/data/loading)
done
echo 1 > /mnt/data/loading



for user in $(cat All_Users); do
twurl "/1.1/statuses/user_timeline.json?screen_name=$user&count=200" > Status_History_Formatted_$user
sleep 3
done
echo 0 > /mnt/data/loading


GET http://115.146.94.29:5984/twitterrawdossr1/_design/Tweet_Ids/_view/all > IDs_R1
GET http://115.146.94.49:5984/twitterrawdossr2/_design/Tweet_Ids/_view/all > IDs_R2
GET http://115.146.93.206:5984/twitterrawdossr3/_design/Tweet_Ids/_view/all > IDs_R3
cat IDs_R1 IDs_R2 IDs_R3 | awk -F"key" '{print $2}'| awk -F"," '{print $1}' | tr -d \": | grep -v ^$ | grep -v null > All_IDs

> All_Tweet_History
for i in $(ls -ltrh Status_History_Formatted_* | awk '{print $9}' | awk '/./'); do
cat $i | sed -e 's/^\[//g' | sed -e 's/\]$//g' | sed -e 's/},{"created_at"/}\n{"created_at"/g' >> All_Tweet_History
echo >> All_Tweet_History
done

fgrep -vf All_IDs All_Tweet_History | sort | uniq > Tweet_History_Filtered


C=1
cat Tweet_History_Filtered | while read -r line; do 

if [ $C -eq 4 ]; then C=1; fi

if [ $C -eq 1 ]; then
echo $line | POST -sS http://115.146.94.29:5984/twitterrawdossr1 -c "application/json" >> Loading_DB1.log
sleep 0.1
fi

if [ $C -eq 2 ]; then
echo $line | POST -sS http://115.146.94.49:5984/twitterrawdossr2 -c "application/json" >> Loading_DB2.log
sleep 0.1
fi

if [ $C -eq 3 ]; then
echo $line | POST -sS http://115.146.93.206:5984/twitterrawdossr3 -c "application/json" >> Loading_DB3.log
sleep 0.1
fi

C=`expr $C + 1`

done
rm *Formatted* Users* IDs_*  
sleep 3600
done &
