#!/bin/bash
source InternationalTwitterHarvesting-Team11-openrc.sh

Node_Name=Timelines
Size=0

./Create_Node.sh $Node_Name $Size > Create_$Node_Name.log
ACTIVE=$(nova list | grep $Node_Name | grep ACTIVE | wc -l)
while [ $ACTIVE -eq 0 ]; do
sleep 3
ACTIVE=$(nova list | grep $Node_Name | grep ACTIVE | wc -l)
done

IP=$(nova list | grep $Node_Name | awk '{print $12}' | awk -F"=" '{print $2}')
Server_ID=$(nova list | grep $Node_Name | awk '{print $2}')
sleep 90 
scp -i /root/.ssh/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -r Dispatcher ubuntu@$IP:~/
scp -i /root/.ssh/key  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no twurltimelines  ubuntu@$IP:~/.twurlrc
./Installer_Timelines.expect $IP > $Node_Name.log 2>&1
