#!/bin/bash
source InternationalTwitterHarvesting-Team11-openrc.sh

Node_Name=node3
Volume1_ID="e7badef9-adf1-42c2-b2fa-4393e904385e"
Volume2_ID="813eb701-bbbe-4992-b32b-113b278825ad"
Size=0

./Create_Node.sh $Node_Name $Size > Create_$Node_Name.log
ACTIVE=$(nova list | grep $Node_Name | grep ACTIVE | wc -l)
while [ $ACTIVE -eq 0 ]; do
sleep 3
ACTIVE=$(nova list | grep $Node_Name | grep ACTIVE | wc -l)
done

IP=$(nova list | grep $Node_Name | awk '{print $12}' | awk -F"=" '{print $2}')
Server_ID=$(nova list | grep $Node_Name | awk '{print $2}')
nova volume-attach $Server_ID $Volume1_ID 
sleep 10
nova volume-attach $Server_ID $Volume2_ID
sleep 90
scp -i /root/.ssh/key  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no twurldb3  ubuntu@$IP:~/.twurlrc
./Installer_Nodes.expect $IP > $Node_Name.log 2>&1
