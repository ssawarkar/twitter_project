#!/bin/bash
source InternationalTwitterHarvesting-Team11-openrc.sh

Node_Name=node2
Volume1_ID="fb2afafb-71e9-4a05-9188-1c3e703469d2"
Volume2_ID="0fd96375-b665-4436-b851-c1ee5ab767c5"
Size=0

./Create_Node.sh $Node_Name $Size > Create_$Node_Name.log
ACTIVE=$(nova list | grep $Node_Name | grep ACTIVE | wc -l)
while [ $ACTIVE -eq 0 ]; do
sleep 3
ACTIVE=$(nova list | grep $Node_Name | grep ACTIVE | wc -l)
done

IP=$(nova list | grep $Node_Name | awk '{print $12}' | awk -F"=" '{print $2}')
Server_ID=$(nova list | grep $Node_Name | awk '{print $2}')
nova volume-attach $Server_ID $Volume1_ID 
sleep 10
nova volume-attach $Server_ID $Volume2_ID
sleep 90
scp -i /root/.ssh/key  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no twurldb2  ubuntu@$IP:~/.twurlrc
./Installer_Nodes.expect $IP > $Node_Name.log 2>&1
