#!/bin/bash
source InternationalTwitterHarvesting-Team11-openrc.sh

Node_Name=HeadNode
Volume_ID="e0e2f813-e6c2-4ff4-8742-79d4f0a22c95"
Size=0

./Create_Node.sh $Node_Name $Size > Create_$Node_Name.log
ACTIVE=$(nova list | grep $Node_Name | grep ACTIVE | wc -l)
while [ $ACTIVE -eq 0 ]; do
sleep 3
ACTIVE=$(nova list | grep $Node_Name | grep ACTIVE | wc -l)
done
sleep 30
IP=$(nova list | grep $Node_Name | awk '{print $12}' | awk -F"=" '{print $2}')
Server_ID=$(nova list | grep $Node_Name | awk '{print $2}')
nova volume-attach $Server_ID $Volume_ID
sleep 90 
./Installer_Head_Node.expect $IP > $Node_Name.log 2>&1
