#!/bin/bash

Graph=Graph5
GET http://db1:5984/twitterrawdocsr1/_design/Active_Period/_view/months > Output_Dublin_$Graph
GET http://db2:5984/twitterrawdocsr2/_design/Active_Period/_view/months >> Output_Dublin_$Graph
GET http://db3:5984/twitterrawdocsr3/_design/Active_Period/_view/months >> Output_Dublin_$Graph


GET http://db1:5984/twitterrawdossr1/_design/Active_Period/_view/months > Output_Melbourne_$Graph
GET http://db2:5984/twitterrawdossr2/_design/Active_Period/_view/months >> Output_Melbourne_$Graph
GET http://db3:5984/twitterrawdossr3/_design/Active_Period/_view/months >> Output_Melbourne_$Graph





cat Output_Dublin_$Graph | awk -F "\"" '{print $8}' | sort | awk '/./'| uniq -c  | awk '{print $2 " " $1}' > P1_Dublin
cat Output_Melbourne_$Graph | awk -F "\"" '{print $8}' | sort | awk '/./' | uniq -c | awk '{print $2 " " $1}' > P1_Melbourne
  


paste P1_Dublin P1_Melbourne | awk '{print $1 " " $2 " " $4}' > P2
>P3
for i in $(cat Template_Months); do grep $i P2 >> P3; done


cat P3 | awk '{print "{\"category\":\""$1"""\",\"column-1\""":"$2",""\"column-2\":"$3"},"}' |  sed '$s/},/}/' > P4

echo "[$(cat P4)]" > $Graph
rm P* Output_*




