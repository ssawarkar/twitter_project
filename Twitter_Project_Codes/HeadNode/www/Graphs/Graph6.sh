#!/bin/bash

Graph=Graph6
GET http://db1:5984/twitterrawdocsr1/_design/Game/_view/local > Output_Dublin_$Graph
GET http://db2:5984/twitterrawdocsr2/_design/Game/_view/local >> Output_Dublin_$Graph
GET http://db3:5984/twitterrawdocsr3/_design/Game/_view/local >> Output_Dublin_$Graph


GET http://db1:5984/twitterrawdossr1/_design/Game/_view/local > Output_Melbourne_$Graph
GET http://db2:5984/twitterrawdossr2/_design/Game/_view/local >> Output_Melbourne_$Graph
GET http://db3:5984/twitterrawdossr3/_design/Game/_view/local >> Output_Melbourne_$Graph





cat Output_Dublin_$Graph | dos2unix | grep -v total_rows |awk '/./' | awk -F "\"" '{print $10" "$8}' | sort -k1 |grep -v ^" "$|uniq -c | grep -v -w  ^"      1" > P1_Dublin 

cat Output_Melbourne_$Graph | dos2unix | grep -v total_rows |awk '/./' | awk -F "\"" '{print $10" "$8}' | sort -k1 |grep -v ^" "$|uniq -c | grep -v -w  ^"      1" > P1_Melbourne


cat P1_Dublin | sed -e 's/ 1$/ ,1/g' | sed -e 's/ -1$/ ,-1/g'  | egrep -v "football|league|rugby|finn|wexford|railway" | awk -F "," '{print $2 $1}' |awk '{print $1*$2" " $3}' > P2_Dublin

cat P1_Melbourne | sed -e 's/ 1$/ ,1/g' | sed -e 's/ -1$/ ,-1/g'  |egrep -v  "football|league|rugby|geelong|sydney" | awk -F "," '{print $2 $1}' |awk '{print $1*$2 " "$3}' > P2_Melbourne


cat P2_Dublin |sed -e '/^[0-9]/{N;s/\n//;}' | sed -e 's/-/ -/g'|awk '{print $2 " " $1 " " $3}' | grep -v gaelic > P3_Dublin
cat P2_Melbourne |sed -e '/^[0-9]/{N;s/\n//;}' | sed -e 's/-/ -/g'|awk '{print $2 " " $1 " " $3}' > P3_Melbourne


cat P3_Dublin| awk '{print "{\"category\":\""$1"""\",\"column-1\""":"$2",""\"column-2\":"$3"},"}' |  sed '$s/},/}/' > P4_Dublin
cat P3_Melbourne| awk '{print "{\"category\":\""$1"""\",\"column-1\""":"$2",""\"column-2\":"$3"},"}' |  sed '$s/},/}/' > P4_Melbourne


echo "[[$(cat P4_Dublin)],[$(cat P4_Melbourne)]]" > $Graph

sed -i "s/gaa/Gaelic/g" $Graph
sed -i "s/gaelic/Gaelic/g" $Graph
sed -i "s/heineken/Heineken/g" $Graph
sed -i "s/hurling/Hurling/g" $Graph
sed -i "s/limerick/Limerick/g" $Graph
sed -i "s/munster/Munster/g" $Graph
sed -i "s/nfl/NFL/g" $Graph
sed -i "s/shamrock/Shamrock/g" $Graph
sed -i "s/shelbourne/Shelbourne/g" $Graph
sed -i "s/ashes/Ashes/g" $Graph
sed -i "s/cats/CATS/g" $Graph
sed -i "s/cricket/Cricket/g" $Graph
sed -i "s/footy/Footy/g" $Graph
sed -i "s/hawks/Hawks/g" $Graph

rm Output_* P*





