#!/bin/bash

Graph=Graph9
GET http://db1:5984/twitterrawdocsr1/_design/Coffee/_view/coffee > Output_Dublin_$Graph
GET http://db2:5984/twitterrawdocsr2/_design/Coffee/_view/coffee >> Output_Dublin_$Graph
GET http://db3:5984/twitterrawdocsr3/_design/Coffee/_view/coffee >> Output_Dublin_$Graph


GET http://db1:5984/twitterrawdossr1/_design/Coffee/_view/coffee > Output_Melbourne_$Graph
GET http://db2:5984/twitterrawdossr2/_design/Coffee/_view/coffee >> Output_Melbourne_$Graph
GET http://db3:5984/twitterrawdossr3/_design/Coffee/_view/coffee >> Output_Melbourne_$Graph


cat Output_Dublin_$Graph |  awk -F"\"" '{print $8 " "$10}' | sort | uniq -ci | sort -k1 -n -r | head > P1_Dublin
cat Output_Melbourne_$Graph |  awk -F"\"" '{print $8 " "$10}' | sort | uniq -ci | sort -k1 -n -r | head > P1_Melbourne

cat P1_Dublin | awk '{print "{\"category\":\""$2 " " $3"""\",\"column-1\""":"$1"},"}'  |  sed '$s/},/}/' > P2_Dublin
cat P1_Melbourne | awk '{print "{\"category\":\""$2" "$3"""\",\"column-1\""":"$1"},"}'  |  sed '$s/},/}/' > P2_Melbourne



echo "[[$(cat P2_Dublin)],[$(cat P2_Melbourne)]]" > $Graph


rm Output_* P*
