#!/bin/bash

Graph=Graph2

GET http://db1:5984/twitterrawdocsr1/_design/Language/_view/used > Output_Dublin_$Graph
GET http://db2:5984/twitterrawdocsr2/_design/Language/_view/used >> Output_Dublin_$Graph
GET http://db3:5984/twitterrawdocsr3/_design/Language/_view/used >> Output_Dublin_$Graph


GET http://db1:5984/twitterrawdossr1/_design/Language/_view/used > Output_Melbourne_$Graph
GET http://db2:5984/twitterrawdossr2/_design/Language/_view/used >> Output_Melbourne_$Graph
GET http://db3:5984/twitterrawdossr3/_design/Language/_view/used >> Output_Melbourne_$Graph



cat Output_Dublin_$Graph|  awk -F"\"" '{print $8}' | sort | uniq -c | sort -k1 -n -r | head -7 > P1

engb=$(grep "en-gb" P1 | awk '{print $1}')
en=$(cat P1 | tr -d - | grep -w en | awk '{print $1}')

EN=$(expr $en + $engb)
cat P1 | grep -v en > P2
#echo "$EN en" >> P2
cat P2 | sort -k1 -n -r > P3_Dublin

cat Output_Melbourne_$Graph |  awk -F"\"" '{print $8}' | sort | uniq -c | sort -k1 -n -r | head -7 > P1

engb=$(grep "en-gb" P1 | awk '{print $1}')
en=$(cat P1 | tr -d - | grep -w en | awk '{print $1}')

EN=$(expr $en + $engb)
cat P1 | grep -v en > P2
#echo "$EN en" >> P2
cat P2 | sort -k1 -n -r > P3_Melbourne


cat P3_Melbourne | awk '{print "{\"category\":\""$2"""\",\"column-1\""":"$1"},"}' |  sed '$s/},/}/' > P4_Melbourne
cat P3_Dublin | awk '{print "{\"category\":\""$2"""\",\"column-1\""":"$1"},"}' |  sed '$s/},/}/' > P4_Dublin




echo "[[$(cat P4_Dublin)]","[$(cat P4_Melbourne)]," > $Graph


sed -i 's/en/English/g' $Graph
sed -i 's/es/Spanish /g' $Graph
sed -i 's/fr/French/g' $Graph
sed -i 's/pt/Portuguese/g' $Graph
sed -i 's/it/Italian/g' $Graph
sed -i 's/ja/Japanese/g' $Graph
sed -i 's/tr/Turkish/g' $Graph
sed -i 's/ar/Arabic/g' $Graph

Lang_Count_Dublin=$(cat Output_Dublin_$Graph|  awk -F"\"" '{print $8}' | sort | uniq | wc -l) 

Lang_Count_Melbourne=$(cat Output_Melbourne_$Graph|  awk -F"\"" '{print $8}' | sort | uniq | wc -l) 

echo '[{"category":"Dublin","column-1":'$Lang_Count_Dublin}, >> $Graph

echo '{"category":"Melbourne","column-1":'$Lang_Count_Melbourne}]] >> $Graph


rm Output* P*
