#!/bin/bash


Count_Dublin_1=$(GET http://db1:5984/twitterrawdocsr1/_design/Tweet_Count/_view/all | grep key | awk -F"value\":" '{print $2}' | tr -d }| dos2unix)
Count_Dublin_2=$(GET http://db2:5984/twitterrawdocsr2/_design/Tweet_Count/_view/all | grep key | awk -F"value\":" '{print $2}' | tr -d }| dos2unix)
Count_Dublin_3=$(GET http://db3:5984/twitterrawdocsr3/_design/Tweet_Count/_view/all | grep key | awk -F"value\":" '{print $2}' | tr -d }| dos2unix)

Count_Melbourne_1=$(GET http://db1:5984/twitterrawdossr1/_design/Tweet_Count/_view/all | grep key | awk -F"value\":" '{print $2}' | tr -d }| dos2unix)
Count_Melbourne_2=$(GET http://db2:5984/twitterrawdossr2/_design/Tweet_Count/_view/all | grep key | awk -F"value\":" '{print $2}' | tr -d }| dos2unix)
Count_Melbourne_3=$(GET http://db3:5984/twitterrawdossr3/_design/Tweet_Count/_view/all | grep key | awk -F"value\":" '{print $2}' | tr -d }| dos2unix)

Count_Dublin=$(expr $Count_Dublin_1 + $Count_Dublin_2 + $Count_Dublin_3)
Count_Melbourne=$(expr $Count_Melbourne_1 + $Count_Melbourne_2 + $Count_Melbourne_3)
Total_Counts=$(expr $Count_Dublin + $Count_Melbourne)

Graph=Graph4
GET http://db1:5984/twitterrawdocsr1/_design/UEType/_view/all > Output_Dublin_$Graph
GET http://db2:5984/twitterrawdocsr2/_design/UEType/_view/all >> Output_Dublin_$Graph
GET http://db3:5984/twitterrawdocsr3/_design/UEType/_view/all >> Output_Dublin_$Graph


GET http://db1:5984/twitterrawdossr1/_design/UEType/_view/all > Output_Melbourne_$Graph
GET http://db2:5984/twitterrawdossr2/_design/UEType/_view/all >> Output_Melbourne_$Graph
GET http://db3:5984/twitterrawdossr3/_design/UEType/_view/all >> Output_Melbourne_$Graph

Android_Dublin=$(cat Output_Dublin_$Graph | grep -i android | wc -l)
Android_Melbourne=$(cat Output_Melbourne_$Graph | grep -i android | wc -l)
Apple_Dublin=$(cat Output_Dublin_$Graph | egrep -i "ios|iphone" | wc -l)
Apple_Melbourne=$(cat Output_Melbourne_$Graph | egrep -i "ios|iphone" | wc -l)

echo '[{"category":"Android","column-1":'$Android_Dublin}, > P1_Dublin
echo '{"category":"Apple","column-1":'$Apple_Dublin}] >> P1_Dublin

echo '[{"category":"Android","column-1":'$Android_Melbourne}, > P2_Melbourne
echo '{"category":"Apple","column-1":'$Apple_Melbourne}] >> P2_Melbourne

echo "[$(cat P1_Dublin),$(cat P2_Melbourne)]" > $Graph
rm P* Output*


