#!/bin/bash

Graph=Graph3
GET http://db1:5984/twitterrawdocsr1/_design/Popular/_view/Popular_Users > Output_Dublin_$Graph
GET http://db2:5984/twitterrawdocsr2/_design/Popular/_view/Popular_Users >> Output_Dublin_$Graph
GET http://db3:5984/twitterrawdocsr3/_design/Popular/_view/Popular_Users >> Output_Dublin_$Graph


GET http://db1:5984/twitterrawdossr1/_design/Popular/_view/Popular_Users > Output_Melbourne_$Graph
GET http://db2:5984/twitterrawdossr2/_design/Popular/_view/Popular_Users >> Output_Melbourne_$Graph
GET http://db3:5984/twitterrawdossr3/_design/Popular/_view/Popular_Users >> Output_Melbourne_$Graph





cat Output_Dublin_$Graph | awk -F"\"" '{print $8" "$9 "" $11}'  | tr -d :\]} | sed -e 's/,/ /g' |  awk '{print $1 " " $2 " " $4}' | sort -k2 -n -r | head -50 > P1_Dublin 

cat Output_Melbourne_$Graph | awk -F"\"" '{print $8" "$9 "" $11}'  | tr -d :\]} | sed -e 's/,/ /g' |  awk '{print $1 " " $2 " " $4}' | sort -k2 -n -r | head -50 > P1_Melbourne


cat P1_Dublin | awk '{print $1}' | sort | uniq > P2_Dublin
> P3_Dublin

for i in $(cat P2_Dublin); do grep $i P1_Dublin | head -1 >> P3_Dublin; done

cat P3_Dublin |  sort -n -k2 -r > P4_Dublin


cat P1_Melbourne | awk '{print $1}' | sort | uniq > P2_Melbourne
> P3_Melbourne

for i in $(cat P2_Melbourne); do grep $i P1_Melbourne | head -1 >> P3_Melbourne; done

cat P3_Melbourne |  sort -n -k2 -r > P4_Melbourne


cat P4_Dublin |  awk '{print "{\"category\":\""$1"""\",\"column-1\""":"$2",""\"column-2\":"$3"},"}' |  sed '$s/},/}/' > P5_Dublin

cat P4_Melbourne |  awk '{print "{\"category\":\""$1"""\",\"column-1\""":"$2",""\"column-2\":"$3"},"}' |  sed '$s/},/}/' > P5_Melbourne

echo "[[$(cat P5_Dublin)]","[$(cat P5_Melbourne)]]" > $Graph


rm P*  Output*
