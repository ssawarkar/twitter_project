#!/bin/bash
Graph=Graph1

#-----------------------------Happy

GET http://db1:5984/twitterrawdocsr1/_design/Sentiment/_view/happy > Output_Dublin_$Graph
GET http://db2:5984/twitterrawdocsr2/_design/Sentiment/_view/happy >> Output_Dublin_$Graph
GET http://db3:5984/twitterrawdocsr3/_design/Sentiment/_view/happy >> Output_Dublin_$Graph


GET http://db1:5984/twitterrawdossr1/_design/Sentiment/_view/happy > Output_Melbourne_$Graph
GET http://db2:5984/twitterrawdossr2/_design/Sentiment/_view/happy >> Output_Melbourne_$Graph
GET http://db3:5984/twitterrawdossr3/_design/Sentiment/_view/happy >> Output_Melbourne_$Graph

Count_Dublin_1=$(GET http://db1:5984/twitterrawdocsr1/_design/Tweet_Count/_view/all | grep key | awk -F"value\":" '{print $2}' | tr -d }| dos2unix)
Count_Dublin_2=$(GET http://db2:5984/twitterrawdocsr2/_design/Tweet_Count/_view/all | grep key | awk -F"value\":" '{print $2}' | tr -d }| dos2unix)
Count_Dublin_3=$(GET http://db3:5984/twitterrawdocsr3/_design/Tweet_Count/_view/all | grep key | awk -F"value\":" '{print $2}' | tr -d }| dos2unix)

Count_Melbourne_1=$(GET http://db1:5984/twitterrawdossr1/_design/Tweet_Count/_view/all | grep key | awk -F"value\":" '{print $2}' | tr -d }| dos2unix)
Count_Melbourne_2=$(GET http://db2:5984/twitterrawdossr2/_design/Tweet_Count/_view/all | grep key | awk -F"value\":" '{print $2}' | tr -d }| dos2unix)
Count_Melbourne_3=$(GET http://db3:5984/twitterrawdossr3/_design/Tweet_Count/_view/all | grep key | awk -F"value\":" '{print $2}' | tr -d }| dos2unix)

Count_Dublin=$(expr $Count_Dublin_1 + $Count_Dublin_2 + $Count_Dublin_3)
Count_Melbourne=$(expr $Count_Melbourne_1 + $Count_Melbourne_2 + $Count_Melbourne_3)



cat Output_Dublin_$Graph | awk -F"key" '{print $2}' | awk '/./' | awk -F"\"" '{print $3 "-" $5}'| tr -d }, | sed -e 's/:/ /g'| sort | uniq -c > P1_Dublin
cat Output_Melbourne_$Graph | awk -F"key" '{print $2}' | awk '/./' | awk -F"\"" '{print $3 "-" $5}'| tr -d }, | sed -e 's/:/ /g'| sort | uniq -c > P1_Melbourne


for i in $(cat Template_Dublin_$Graph); do grep $i P1_Dublin >> P2_Dublin; done
for i in $(cat Template_Melbourne_$Graph); do grep $i P1_Melbourne >> P2_Melbourne; done

cat P2_Dublin | awk '{print $1}' > Dublin_Values
cat P2_Melbourne | awk '{print $1}' > Melbourne_Values
cat P2_Dublin  | awk '{print $2}' > Category

for i in $(cat Dublin_Values); do echo "scale=3; $i*100/$Count_Dublin" | bc >> PD; done
for i in $(cat Melbourne_Values); do j=$(echo "scale=3; $i*100/$Count_Melbourne" | bc); echo $j >> PM; done

paste Category PD PM > P3

cat P3 | awk '{print "{\"category\":\""$1"""\",\"column-1\""":"$2",""\"column-2\":"$3"},"}' |  sed '$s/},/}/' > P4_Happy


rm Output_* Melbourne_Values Dublin_Values Category PD PM P1* P2* P3*

#-----------------------------------SAD

GET http://db1:5984/twitterrawdocsr1/_design/Sentiment/_view/sad > Output_Dublin_$Graph
GET http://db2:5984/twitterrawdocsr2/_design/Sentiment/_view/sad >> Output_Dublin_$Graph
GET http://db3:5984/twitterrawdocsr3/_design/Sentiment/_view/sad >> Output_Dublin_$Graph


GET http://db1:5984/twitterrawdossr1/_design/Sentiment/_view/sad > Output_Melbourne_$Graph
GET http://db2:5984/twitterrawdossr2/_design/Sentiment/_view/sad >> Output_Melbourne_$Graph
GET http://db3:5984/twitterrawdossr3/_design/Sentiment/_view/sad >> Output_Melbourne_$Graph




cat Output_Dublin_$Graph | awk -F"key" '{print $2}' | awk '/./' | awk -F"\"" '{print $3 "-" $5}'| tr -d }, | sed -e 's/:/ /g'| sort | uniq -c > P1_Dublin
cat Output_Melbourne_$Graph | awk -F"key" '{print $2}' | awk '/./' | awk -F"\"" '{print $3 "-" $5}'| tr -d }, | sed -e 's/:/ /g'| sort | uniq -c > P1_Melbourne


for i in $(cat Template_Dublin_$Graph); do grep $i P1_Dublin >> P2_Dublin; done
for i in $(cat Template_Melbourne_$Graph); do grep $i P1_Melbourne >> P2_Melbourne; done

cat P2_Dublin | awk '{print $1}' > Dublin_Values
cat P2_Melbourne | awk '{print $1}' > Melbourne_Values
cat P2_Dublin  | awk '{print $2}' > Category

for i in $(cat Dublin_Values); do echo "scale=3; $i*100/$Count_Dublin" | bc >> PD; done
for i in $(cat Melbourne_Values); do j=$(echo "scale=3; $i*100/$Count_Melbourne" | bc); echo $j >> PM; done

paste Category PD PM > P3

cat P3 | awk '{print "{\"category\":\""$1"""\",\"column-1\""":"$2",""\"column-2\":"$3"},"}' |  sed '$s/},/}/' > P4_Sad


echo "[[$(cat P4_Happy)]","[$(cat P4_Sad)]]" > $Graph


rm Output_* Melbourne_Values Dublin_Values Category PD PM P1* P2* P3* P4*


