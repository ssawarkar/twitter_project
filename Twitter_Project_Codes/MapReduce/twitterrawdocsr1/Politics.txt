{
   "_id": "_design/Politics",
   "_rev": "1-de1f2d3aeec8c224176b40b97b49b785",
   "language": "javascript",
   "views": {
       "used": {
           "map": "function(doc) {\n\tvar dublin = /parliament|democracy|politics|prime minister|fine gael|enda kenny|labor party|eamin gillmore|michael higgins|political system|clientelist|government|\\selection|legislature|legislation|polity|statecraft|state affairs|domestic affairs|foreign affairs|political|bureaucracy|governance|jurisdiction|ministry|presidency|president|sovereignty|bureaucracy|minister/gi;\n\tvar yes = doc.text.split(/[\\s*\\.*\\,\\;\\+?\\#\\|@:\\-\\/\\\\\\[\\]\\(\\)\\{\\}\\\"*$%&0-9*]/);\n\tvar rep = /[!*|\\.*]/gi;\n\tfor ( var i=0; i< yes.length; i++) {\n\t\tif (yes[i].toLowerCase().match(dublin)) {\n\t\t\tyes[i] = yes[i].replace(rep,\"\");\n\t\t\temit(yes[i].toLowerCase(),doc.text)\n\t\t}\n\t}\n}",
           "reduce": "_count"
       }
   }
}