#!/bin/bash
twurl -H stream.twitter.com -X POST "/1.1/statuses/filter.json?locations=144.206501,-38.519931,145.572880,-38.226738" >> /mnt/data/Melbourne/Raw_Tweets.json &

while true; do
File_Size=$(ls -ltr /mnt/data/Melbourne/Raw_Tweets.json | awk '{print $5}')

if [ $File_Size -gt 499999 ]; then
cp /mnt/data/Melbourne/Raw_Tweets.json /mnt/data/Melbourne/store/Raw_Tweets_Uploading.json
> /mnt/data/Melbourne/Raw_Tweets.json
cat /mnt/data/Melbourne/store/Raw_Tweets_Uploading.json | grep ^'{"created_at"' |  awk -F"id_str" '{print $2}' | awk -F"," '{print $1}'| tr -d \":  > /mnt/data/Melbourne/IDs

loading_DB2=$(ssh -i /home/ubuntu/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@db2 'cat /mnt/data/Melbourne/loading')
loading_DB3=$(ssh -i /home/ubuntu/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@db3 'cat /mnt/data/Melbourne/loading')

while [ $loading_DB2 -eq 1 ]||[ $loading_DB3 -eq 1 ]; do  
sleep 3
loading_DB2=$(ssh -i /home/ubuntu/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@db2 'cat /mnt/data/Melbourne/loading')
loading_DB3=$(ssh -i /home/ubuntu/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@db3 'cat /mnt/data/Melbourne/loading')
done

echo 1 > /mnt/data/Melbourne/loading
for id in $(cat /mnt/data/Melbourne/IDs); do
Q_DB1=`GET http://115.146.94.29:5984/twitterrawdossr1/_design/Tweet_Ids/_view/all?key=\"$id\" | grep $id | wc -l`
Q_DB2=`GET http://115.146.94.49:5984/twitterrawdossr2/_design/Tweet_Ids/_view/all?key=\"$id\" | grep $id | wc -l`
Q_DB3=`GET http://115.146.93.206:5984/twitterrawdossr3/_design/Tweet_Ids/_view/all?key=\"$id\" | grep $id | wc -l`
if [ $Q_DB1 -eq 0 ]&&[ $Q_DB2 -eq 0 ]&&[ $Q_DB3 -eq 0 ]; then
Tweet=$(grep $id /mnt/data/Melbourne/store/Raw_Tweets_Uploading.json | grep ^'{"created_at')
echo "Loading the following tweet: $Tweet" >> /mnt/data/Melbourne/Loading.log 
echo $Tweet | POST -sS http://127.0.0.1:5984/twitterrawdossr1 -c "application/json" >> /mnt/data/Melbourne/Loading.log 
fi
done
echo 0 > /mnt/data/Melbourne/loading

mv /mnt/data/Melbourne/store/Raw_Tweets_Uploading.json /mnt/data/Melbourne/store/Raw_Tweets_$(date +%s).json
fi

File_Count=$(ls -l /mnt/data/Melbourne/store/Raw_Tweets_* | wc -l)
if [ $File_Count -gt 100 ]; then
To_Be_Del=$(ls -ltr /mnt/data/Melbourne/store/Raw_Tweets_* | head -1 | awk '{print $9}')
rm -rf /mnt/data/Melbourne/store/$To_Be_Del
fi
sleep 3

done &
