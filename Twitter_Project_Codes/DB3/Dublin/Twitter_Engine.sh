#!/bin/bash
twurl -H stream.twitter.com -X POST "/1.1/statuses/filter.json?locations=-6.382134,53.386795,-6.027389,53.411006" >> /mnt/data/Dublin/Raw_Tweets.json &

while true; do
File_Size=$(ls -ltr /mnt/data/Dublin/Raw_Tweets.json | awk '{print $5}')

if [ $File_Size -gt 499999 ]; then
cp /mnt/data/Dublin/Raw_Tweets.json /mnt/data/Dublin/store/Raw_Tweets_Uploading.json
> /mnt/data/Dublin/Raw_Tweets.json
cat /mnt/data/Dublin/store/Raw_Tweets_Uploading.json | grep ^'{"created_at"' |  awk -F"id_str" '{print $2}' | awk -F"," '{print $1}'| tr -d \":  > /mnt/data/Dublin/IDs

loading_DB1=$(ssh -i /home/ubuntu/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@db1 'cat /mnt/data/Dublin/loading')
loading_DB2=$(ssh -i /home/ubuntu/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@db2 'cat /mnt/data/Dublin/loading')

while [ $loading_DB1 -eq 1 ]||[ $loading_DB2 -eq 1 ]; do
sleep 3
loading_DB1=$(ssh -i /home/ubuntu/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@db1 'cat /mnt/data/Dublin/loading')
loading_DB2=$(ssh -i /home/ubuntu/key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  ubuntu@db2 'cat /mnt/data/Dublin/loading')
done

echo 1 > /mnt/data/Dublin/loading
for id in $(cat /mnt/data/Dublin/IDs); do
Q_DB1=`GET http://db1:5984/twitterrawdocsr1/_design/Tweet_Ids/_view/all?key=\"$id\" | grep $id | wc -l`
Q_DB2=`GET http://db2:5984/twitterrawdocsr2/_design/Tweet_Ids/_view/all?key=\"$id\" | grep $id | wc -l`
Q_DB3=`GET http://db3:5984/twitterrawdocsr3/_design/Tweet_Ids/_view/all?key=\"$id\" | grep $id | wc -l`
if [ $Q_DB1 -eq 0 ]&&[ $Q_DB2 -eq 0 ]&&[ $Q_DB3 -eq 0 ]; then
Tweet=$(grep $id /mnt/data/Dublin/store/Raw_Tweets_Uploading.json | grep ^'{"created_at')
echo "Loading the following tweet: $Tweet" >> /mnt/data/Dublin/Loading.log 
echo $Tweet | POST -sS http://127.0.0.1:5984/twitterrawdocsr3 -c "application/json" >> /mnt/data/Dublin/Loading.log 
fi
done
echo 0 > /mnt/data/Dublin/loading

mv /mnt/data/Dublin/store/Raw_Tweets_Uploading.json /mnt/data/Dublin/store/Raw_Tweets_$(date +%s).json
fi

File_Count=$(ls -l /mnt/data/Dublin/store/Raw_Tweets_* | wc -l)
if [ $File_Count -gt 100 ]; then
To_Be_Del=$(ls -ltr /mnt/data/Dublin/store/Raw_Tweets_* | head -1 | awk '{print $9}')
rm -rf /mnt/data/Dublin/store/$To_Be_Del
fi
sleep 3

done &
